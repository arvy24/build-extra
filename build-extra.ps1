﻿function Set-FrameworkEnvironment
{
    param(
        [parameter(Mandatory=$true)]
        [string]$version,
        [validateset("AMD64","x64","x86")]
        [string]$arch="AMD64")

    # http://msdn.microsoft.com/library/y549e41e.aspx

    if ($arch -eq "x64") { $arch = "AMD64"; }

    $path = [System.IO.Path]::Combine($env:SystemRoot, "Microsoft.NET")
    switch($arch)
    {
        "AMD64" { $path = [System.IO.Path]::Combine($path, "Framework64"); break; }
        "x86"   { $path = [System.IO.Path]::Combine($path, "Framework");   break; }
    }
    
    # friendlier version number
    switch($version)
    {
        "v2"    { $version = "v2.0.50727"; break; }
        "v2.0"  { $version = "v2.0.50727"; break; }
        "v3"    { $version = "v3.5";       break; }
        "v3.0"  { $version = "v3.5";       break; }
        "v4"    { $version = "v4.0.30319"; break; }
        "v4.0"  { $version = "v4.0.30319"; break; }
        "v4.5"  { $version = "v4.0.30319"; break; }
    }

    $path = [System.IO.Path]::Combine($path, $version);

    if (Test-Path $path)
    {
        $env:Path = "$path;" + $env:Path
    }
    else
    {
        Write-Error $errorMessage = "Cannot find dotnet framework $version ($arch)" -Category InvalidArgument
    }
}

function Set-VisualStudioEnvironment
{
    param(
        [parameter(Mandatory=$true)]
        [string]$version,
        [validateset("AMD64","x64","x86","x86_amd64","x86_arm")]
        [string]$arch="AMD64")
    
    if ($arch -eq "x64") { $arch = "AMD64"; }

    # friendlier version number
    switch($version.ToUpper())
    {
        "8"      { $version = "8.0";  break; }
        "2005"   { $version = "8.0";  break; }
        "VS2005" { $version = "8.0";  break; }
        
        "9"      { $version = "9.0";  break; }
        "2008"   { $version = "9.0";  break; }
        "VS2008" { $version = "9.0";  break; }
        
        "10"     { $version = "10.0"; break; }
        "2010"   { $version = "10.0"; break; }
        "VS2010" { $version = "10.0"; break; }
        
        "11"     { $version = "11.0"; break; }
        "2012"   { $version = "11.0"; break; }
        "VS2012" { $version = "11.0"; break; }
    }
    
    $path = ""

    # check environment variable
    $envVariableName = "VS" + $version.Replace(".","") + "COMNTOOLS"
    if (Test-Path "env:$envVariableName")
    {
        $envVariable = Get-Item -path "env:$envVariableName"

        if ($envVariable -ne $null)
        {
            $path = $envVariable.Value
            Write-Debug "Detected Visual Studio from environment variable: $envVariableName"
        }
    }

    # fall back using registry
    if ($path -eq "")
    {
        $key = "HKLM:\SOFTWARE\Microsoft\VisualStudio\$version"
        if (Test-Path $key)
        {
            $keyProperty = Get-ItemProperty $key
            if ($keyProperty.InstallDir -ne $null)
            {
                $path = $keyProperty.InstallDir
                Write-Debug Detected Visual Studio from registry: $key
            }
        }
    }

    # try Wow6432Node
    if ($path -eq "")
    {
        $key = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\VisualStudio\$version"
        if (Test-Path $key)
        {
            $keyProperty = Get-ItemProperty $key
            if ($keyProperty.InstallDir -ne $null)
            {
                $path = $keyProperty.InstallDir
                Write-Debug Detected Visual Studio from registry: $key
            }
        }
    }

    if ($path -ne "")
    {
        $path = $path + "..\..\VC\vcvarsall.bat"
        Write-Debug $path
        if (Test-Path $path)
        {
            Write-Debug "Found vcvarsall.bat"

            try
            {
                Set-EnvironmentWithBatchfile $path $arch
            }
            catch
            {
                Write-Error "Failed to set Visual Studio environment" -Category InvalidOperation
            }
        }
    }
    else
    {
        Write-Error "Visual Studio $version not installed" -Category NotInstalled
    }    
}

function Set-WindowsSDKEnvironment
{
    param(
        [parameter(Mandatory=$true)]
        [string]$version,
        [validateset("AMD64","x64","x86")]
        [string]$arch="AMD64")
    
    if ($arch -eq "x64") { $arch = "AMD64"; }

    $key = "HKLM:\SOFTWARE\Microsoft\Microsoft SDKs\Windows\$version"

    if ($arch -ne "AMD64")
    {
        $key = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Microsoft SDKs\Windows\$version"
    }

    $path = ""

    if (Test-Path $key)
    {
        $keyProperty = Get-ItemProperty $key
        if ($keyProperty.InstallationFolder -ne $null)
        {
            $path = [System.IO.Path]::Combine($keyProperty.InstallationFolder, "bin")
        }
    }

    if ($path -ne "")
    {
        if (Test-Path $path)
        {
            $env:Path = "$path;" + $env:Path
        }
    }
    else
    {
        Write-Error "Windows SDK $version not installed" -Category NotInstalled
    }
}

function Set-EnvironmentWithBatchfile
{
    param(
        [parameter(Position = 0, Mandatory=$true)]
        [string]$path,
        [parameter(Position = 1, Mandatory=$true)]
        [string]$arch)

    $command = "`"$path`" $arch & set"
    cmd /c $command | Foreach-Object { $envVariable, $envValue = $_.split('='); Set-Item -path env:$envVariable -value $envValue; }
}

function Update-AssemblyInfo
{
    param(
        [Parameter(Position = 0, Mandatory = $true)]
        [Hashtable]$properties,
        [Parameter(Position = 1, Mandatory = $true)]
        [String[]]$files)
    
    $rx = New-Object Regex( 
        "^[ `t]*\[assembly: Assembly(?<key>\w+)\(.*\)\](?<other>.*)$", 
        [System.Text.RegularExpressions.RegexOptions](
            [System.Text.RegularExpressions.RegexOptions]::Multiline -bor 
            [System.Text.RegularExpressions.RegexOptions]::Compiled))
    
    foreach($file in $files)
    {
        if (Test-Path $file)
        {
            $content = ""
            
            # get-content returns an array of strings, each item in the array represent a line of text
            # combine array of string to form a single string separated by line feed
            $array = Get-Content $file
            for($i = 0; $i -lt $array.Length; $i++)
            {
                if ($i -lt ($array.Length - 1))
                {
                    $content += $array[$i] + "`n"
                }
                else
                {
                    $content += $array[$i]
                }
            }
            
            $content = $rx.Replace($content,
                {
                    param($m)
                    
                    $key = $m.Groups["key"].Value
                    $value = $properties[$key]
                    
                    if ($value -ne $null)
                    {
                        return "[assembly: Assembly" + $key + "(""" + $value + """)]" + $m.Groups["other"].Value
                    }
                    else { return $m.value }
                })
            
            # split string by line feed to array of strings
            # so it can be written back using Set-Content
            Set-Content -path $file -value ($content.split("`n"))
        }
        else
        {
            Write-Warning "$file does not exists"
        }
    }
}

function Get-GitCommit
{
    $gitLog = git log --oneline -1
    return $gitLog.Split(' ')[0]
}
